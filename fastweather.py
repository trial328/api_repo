# main.py

import requests
import json
from urllib import request
from fastapi import FastAPI
from pydantic import BaseModel

class Weather(BaseModel):
    temp: int
    pressure: int
    humidity: int
    description: str
    country: str

api_key = "e2314f789baa1493ac5e0afb28328167"

wurl = "https://api.openweathermap.org/data/2.5/weather?"

app = FastAPI()

@app.get("/{city}",response_model=Weather)
async def read_city(weather:Weather):
    city = input("Enter the City : ")
    fin_url = wurl + "q=" + city + "&appid=" + api_key
    response=requests.get(fin_url)
    weather=response.json()

    return {"Temperature : ",weather.temp, "\n Pressure : ",weather.pressure, "\n Humidity : ",weather.humidity, 
    "\n Description : ",weather.description,"\n Country : ",weather.country}
