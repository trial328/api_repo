
from unicodedata import name
import mysql.connector
import requests
from fastapi import FastAPI
from pydantic import BaseModel
import json

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="root"
)

class Emp:
    def Emp(self,name,age):
        self.name=name
        self.age=age


#class emp(BaseModel):
#    name: str
#    age: int

#print(mydb)

app = FastAPI()

@app.get("/")
def new_emp():
    mycursor = mydb.cursor()
    mycursor.execute("use dvdb")
    stmt=("select * from employee")
    mycursor.execute(stmt)
    li=list()
    for x in mycursor:
        li.append(x)
    #jli=json.dumps(li)

    nli=[]
    ali=[]
    di={}
    for x in range(len(li)):
        nli.append(li[x][0])
    for x in range(len(li)):
        ali.append(li[x][1])
    #di.update({'name':li[x][0]})


    #print(nli)
    #print(ali)
    di['name']=nli
    di['age']=ali
    #print(di)
    jli=json.dumps(di)
    return jli

## INSERT DATA 
@app.post("/{name}/{age}")
def insert_emp(name:str,age:int):
    mycursor = mydb.cursor()
    mycursor.execute("use dvdb")
    stmt=("insert into employee values (%s, %s)")
    data = (name,age)
    mycursor.execute(stmt,data)
    mycursor.execute("commit")

@app.put("/{name}/{age}")
def update_emp(name:str,age:int):
    mycursor = mydb.cursor()
    mycursor.execute("use dvdb")
    stmt=("update employee set age = (%s) where name = (%s)")
    data = (age,name)
    mycursor.execute(stmt,data)
    mycursor.execute("commit")



@app.delete("/del/{name}/{age}")
def delete(name:str,age:int):            
  cur = mydb.cursor()    
  cur.execute("use dvdb")
  query = "DELETE FROM employee where (name=(%s) and age=(%s))"  
  data=(name,age)
  cur.execute(query,data)   
  print(query)
  cur.execute("commit")       

